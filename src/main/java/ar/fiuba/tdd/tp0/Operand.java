package ar.fiuba.tdd.tp0;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;
import java.util.function.Function;

public class Operand {

    public static final Map<String, Function<Stack<Float>, Float>> FUNCTIONS_MAP = new HashMap<String, Function<Stack<Float>, Float>>();

    static {

        FUNCTIONS_MAP.put("+", (Stack<Float> numStack) -> {
                validateStackForBinaryOperand(numStack);
                float secondAdded = numStack.pop();
                float firstAdded = numStack.pop();
                return firstAdded + secondAdded;
            });

        FUNCTIONS_MAP.put("-", (Stack<Float> numStack) -> {
                validateStackForBinaryOperand(numStack);
                float secondAdded = numStack.pop();
                float firstAdded = numStack.pop();
                return firstAdded - secondAdded;
            });
    }

    static {
        FUNCTIONS_MAP.put("/", (Stack<Float> numStack) -> {
                validateStackForBinaryOperand(numStack);
                float secondAdded = numStack.pop();
                float firstAdded = numStack.pop();
                return firstAdded / secondAdded;
            });

        FUNCTIONS_MAP.put("*", (Stack<Float> numStack) -> {
                validateStackForBinaryOperand(numStack);
                float secondAdded = numStack.pop();
                float firstAdded = numStack.pop();
                return firstAdded * secondAdded;
            });
    }

    static {
        FUNCTIONS_MAP.put("MOD", (Stack<Float> numStack) -> {
                validateStackForBinaryOperand(numStack);
                float secondAdded = numStack.pop();
                float firstAdded = numStack.pop();
                return firstAdded % secondAdded;
            });

        FUNCTIONS_MAP.put("++", (Stack<Float> numStack) -> {
                validateStackForNAryOperand(numStack);
                Float resPlus = numStack.pop();
                while (!numStack.isEmpty()) {
                    resPlus += numStack.pop();
                }
                return resPlus;
            });
    }

    static {
        FUNCTIONS_MAP.put("--", (Stack<Float> numStack2) -> {
                validateStackForNAryOperand(numStack2);
                Float resSub = numStack2.pop();
                while (!numStack2.isEmpty()) {
                    resSub -= numStack2.pop();
                }
                return resSub;
            });

        FUNCTIONS_MAP.put("**", (Stack<Float> numStack3) -> {
                validateStackForNAryOperand(numStack3);
                return performMultipleMultiply(numStack3);
            });
    }

    static {
        FUNCTIONS_MAP.put("//", (Stack<Float> numStack4) -> {
                validateStackForNAryOperand(numStack4);
                Float resDiv = numStack4.pop();
                while (!numStack4.isEmpty()) {
                    resDiv /= numStack4.pop();
                }
                return resDiv;
            });
    }

    private static void validateStackForBinaryOperand(Stack<Float> numStack) {
        Optional.of(numStack.size()).filter(i -> i >= 2).orElseThrow(() -> new IllegalArgumentException());
    }

    private static void validateStackForNAryOperand(Stack<Float> numStack) {
        Optional.of(numStack.size()).filter(i -> i >= 1).orElseThrow(() -> new IllegalArgumentException());
    }

    private static Float performMultipleMultiply(Stack<Float> numStack) {
        Float resMul = numStack.pop();
        while (!numStack.isEmpty()) {
            resMul *= numStack.pop();
        }
        return resMul;
    }

}
