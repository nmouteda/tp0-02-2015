package ar.fiuba.tdd.tp0;

import java.util.Arrays;
import java.util.Optional;
import java.util.Stack;
import java.util.function.Function;

public class RPNCalculator {

    public float eval(String expression) {
        Optional.ofNullable(expression).orElseThrow(() -> new IllegalArgumentException());

        Stack<Float> opStack = new Stack<>();
        Arrays.asList(expression.split(" ")).stream().forEach(op -> {
                try {
                    opStack.push(Float.parseFloat(op));
                } catch (NumberFormatException nfe) {
                    Optional.ofNullable(Operand.FUNCTIONS_MAP.get(op)).orElseThrow(() -> new IllegalArgumentException());
                    calcSign(opStack, Operand.FUNCTIONS_MAP.get(op));
                }
            });
        return opStack.pop();
    }

    protected static Stack<Float> calcSign(Stack<Float> opStack, Function<Stack<Float>, Float> op) {
        opStack.push(op.apply(opStack));
        return opStack;
    }
}
